import '../styles/globals.css'
import Protocol from 'dark-crystal-web3-transport'
import { useRouter } from 'next/router'
const log = console.log

function MyApp({ Component, pageProps }) {
  const router = useRouter()

  // To allow us to pretend to be multiple users in different tabs when testing
  const userId = router.query?.user
  pageProps.userId = userId

  if (typeof window !== 'undefined' && userId && !pageProps.protocol) {
    // Temporary - this would be the seed from signing a standard message
    const seed =
      localStorage.getItem(userId + 'seed') ||
      Protocol.generateSeed().toString('hex')
    localStorage.setItem(userId + 'seed', seed)
    log('Using seed', seed, ' User id: ', userId)

    // Temporary - this would be actual eth address from web3 api
    const ethAddress = '0x' + seed.slice(24)

    pageProps.protocol = new Protocol({
      seed: Buffer.from(seed, 'hex'),
      ethAddress,
    })

    pageProps.ethAddressesFromStorage = JSON.parse(
      localStorage.getItem(userId + 'ethAddresses') || '[]'
    )
    for (const ethAddress of pageProps.ethAddressesFromStorage) {
      pageProps.protocol.addEthAddress(ethAddress)
    }
  }
  return <Component {...pageProps} />
}

export default MyApp
