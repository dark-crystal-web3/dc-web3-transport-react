import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Protocol from 'dark-crystal-web3-transport'
import React from 'react'
import Share from '../components/share.js'
import h from 'react-hyperscript'
import * as dc from '../shared/share-recover.js'
import isEthAddress from '../shared/is-eth-address.js'
import Link from 'next/link'
const log = console.log

export default class Home extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      peerId: 'Connecting to waku...',
      numberOfRelayPeers: 0,
      storePeers: JSON.stringify([]),
      publicKey: '',
      ethAddress: '',
      ethAddresses: [],
      selectedPublicKeys: [],
      newEthAddress: '',
      canAddEthAddress: false,
      shares: [],
      shareRequests: [],
    }

    this.updateEthAddress = this.updateEthAddress.bind(this)
    this.addEthAddress = this.addEthAddress.bind(this)
    this.sendPublicKey = this.sendPublicKey.bind(this)
    this.queryHistory = this.queryHistory.bind(this)
  }

  componentDidMount() {
    log('Component did mount')
    if (this.props.protocol) this.setListeners(this.props.protocol)
  }

  componentDidUpdate(prevProps) {
    log('Component did update')
    if (this.props.protocol && !prevProps.protocol) {
      this.setListeners(this.props.protocol)
    }
  }

  setListeners(protocol) {
    log('Setting listeners')
    protocol.on('ready', () => {
      this.setState({
        peerId: protocol.wakuClient?.peerId || 'Connecting to waku...',
        publicKey: protocol.messageHandler.keypair.publicKey.toString('hex'),
        ethAddress: protocol.ethAddress,
        ethAddresses: protocol.getEthAddresses(),
      })
      // this.queryHistory()
    })

    protocol.on('update', () => {
      this.setState({
        ethAddresses: protocol.getEthAddresses(),
        shareRequests: protocol.getShareRequests(),
        shares: protocol.getShares(),
      })
    })

    protocol.on('relayPeers', (numberOfRelayPeers) => {
      if (this.state.numberOfRelayPeers !== numberOfRelayPeers) {
        this.setState({ numberOfRelayPeers })
      }
    })

    protocol.on('storePeers', (storePeers) => {
      this.setState({ storePeers: JSON.stringify(storePeers) })
    })
  }

  queryHistory() {
    this.props.protocol.wakuClient.queryHistory()
  }

  updateEthAddress(event) {
    this.setState({
      newEthAddress: event.target.value,
      canAddEthAddress: isEthAddress(event.target.value),
    })
  }

  async addEthAddress() {
    await this.props.protocol.addEthAddress(this.state.newEthAddress)
    const ethAddressesFromStorage = this.props.ethAddressesFromStorage
    if (!ethAddressesFromStorage.includes(this.state.newEthAddress)) {
      ethAddressesFromStorage.push(this.state.newEthAddress)
      localStorage.setItem(
        this.props.userId + 'ethAddresses',
        JSON.stringify(ethAddressesFromStorage)
      )
    }
    this.props.protocol.wakuClient.queryHistory()
    const ethAddresses = this.props.protocol.getEthAddresses()
    this.setState({
      ethAddresses,
      newEthAddress: '',
    })
  }

  sendPublicKey() {
    this.props.protocol.sendPublicKey()
  }

  render() {
    const self = this

    function notMine({ ethAddress }) {
      return ethAddress !== self.state.ethAddress
    }

    return h('div', { className: styles.container }, [
      h(Head, [
        h('title', 'Dark Crystal Web3 Transport'),
        h('link', { rel: 'icon', href: '/favicon.ico' }),
      ]),
      h('main', [
        this.props.userId
          ? h('p', ['User id ', h('code', this.props.userId)])
          : h('h2', [
              'No user Id given - which to use? ',
              [
                'secret-owner',
                'recovery-partner-1',
                'recovery-partner-2',
                'recovery-partner-3',
              ].map((userId) => {
                return h(
                  Link,
                  { href: `/?user=${userId}` },
                  h('button', userId)
                )
              }),
            ]),
        h('p', ['Peer id ', h('code', this.state.peerId)]),
        h('p', ['Relay peers ', this.state.numberOfRelayPeers]),
        h('p', ['Store peers', h('code', this.state.storePeers)]),
        h('div', [
          h(
            'button',
            { onClick: this.queryHistory },
            'Re-check store for messages'
          ),
        ]),
        h('hr'),
        h('p', ['Public Encryption Key: ', h('code', this.state.publicKey)]),
        h('div', [
          h('p', [
            'Your eth address: ',
            h('code', this.state.ethAddress),
            ' ',
            this.state.ethAddress.length
              ? h(
                  'button',
                  { onClick: this.sendPublicKey },
                  '🗝 Publish public key'
                )
              : undefined,
            displayOwnEthAddressPublished(),
          ]),
        ]),
        h('h3', 'Known Eth addresses'),
        h('ul', this.state.ethAddresses.filter(notMine).map(displayEthAddress)),
        h('div', [
          h('input', {
            type: 'text',
            size: '32',
            value: this.state.newEthAddress,
            onChange: this.updateEthAddress,
            placeholder: "Add a recovery partner's eth address",
          }),
          h(
            'button',
            {
              onClick: this.addEthAddress,
              disabled: !this.state.canAddEthAddress,
            },
            'Add'
          ),
          this.state.newEthAddress.length && !this.state.canAddEthAddress
            ? h('small', ['Must be formatted ', h('code', '0x<20 bytes hex>')])
            : undefined,
        ]),
        h(Share, { selectedPublicKeys: this.state.selectedPublicKeys }),
        h('h3', 'Received share requests'),
        h('ul', this.state.shareRequests.map(displayShareRequest)),
        h('h3', 'Received shares'),
        h('ul', this.state.shares.map(displayShare)),
      ]),
    ])

    // TODO these should probably be react components
    function displayEthAddress({ ethAddress, timestamp, publicKey }) {
      const onChange = (event) => {
        const selected = new Set(self.state.selectedPublicKeys)
        if (event.target.checked) {
          selected.add(publicKey.toString('hex'))
        } else {
          selected.delete(publicKey.toString('hex'))
        }
        self.setState({ selectedPublicKeys: Array.from(selected) })
      }

      return h('li', { key: ethAddress }, [
        h('code', ethAddress),
        publicKey
          ? h('span', [
              h('input', {
                type: 'checkbox',
                onChange,
                checked: self.state.selectedPublicKeys.includes(
                  publicKey.toString('hex')
                ),
              }),
              '🗝 published ',
              new Date(timestamp).toLocaleString(),
              ' ',
              h(
                'button',
                {
                  onClick: () => {
                    self.props.protocol.sendShareRequest(
                      self.props.protocol.ethAddress,
                      ethAddress
                    )
                  },
                },
                '💎 Request a share from this peer'
              ),
            ])
          : h('span', 'Waiting for public key...'),
      ])
    }

    function displayShareRequest({ lookupKey, timestamp }) {
      return h('li', { key: lookupKey + timestamp }, [
        h('code', lookupKey),
        ' published ',
        new Date(timestamp).toLocaleString(),
        ' ',
        h(
          'button',
          {
            onClick: () => {
              self.props.protocol.sendShare(
                lookupKey,
                Buffer.from('This is a share')
              )
            },
          },
          '💎 Respond with share'
        ),
      ])
    }

    function displayShare({ share, timestamp }) {
      return h('li', { key: share.toString() }, [
        '🌟 ',
        h('code', share.toString()),
        ' published ',
        new Date(timestamp).toLocaleString(),
      ])
    }

    function displayOwnEthAddressPublished() {
      if (!self.state.ethAddress.length) return
      const me = self.state.ethAddresses.find(({ ethAddress }) => {
        return ethAddress === self.state.ethAddress
      })
      if (me && me.publicKey) {
        return h('span', [
          '🗝 published ',
          new Date(me.timestamp).toLocaleString(),
        ])
      }
    }
  }
}
