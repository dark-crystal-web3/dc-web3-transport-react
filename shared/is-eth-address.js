module.exports = function (ethAddress) {
  if (typeof ethAddress !== 'string') return false
  return ethAddress.slice(0, 2) === '0x' &&
    Buffer.from(ethAddress.slice(2), 'hex').length === 20
}
