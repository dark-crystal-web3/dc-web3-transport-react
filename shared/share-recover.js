import * as dcShare from 'web3-backup-js-binding'

function share(publicKeysHex, secret, lookupKey) {
  const output = JSON.parse(
    dcShare.share(
      JSON.stringify({
        public_keys: publicKeysHex,
        secret,
        lookup_key: lookupKey,
      })
    )
  )
  if (output.Error) throw new Error(output.Error)
  return output.Success
}

function recover(shares) {
  const output = JSON.parse(dcShare.recover(JSON.stringify({ shares })))
  if (output.Error) throw new Error(output.Error)
  return output.Success
}

export { share, recover }
