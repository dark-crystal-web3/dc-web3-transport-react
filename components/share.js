import React from 'react'
import h from 'react-hyperscript'
import * as dc from '../shared/share-recover.js'

/// For creating backups

export default class Share extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      secret: '',
      lookupKey: '',
      shareLink: '',
    }
    this.updateSecret = this.updateSecret.bind(this)
    this.updateLookupKey = this.updateLookupKey.bind(this)
    this.createBackup = this.createBackup.bind(this)
  }

  render() {
    return h('div', [
      h('div', [
        h('label', 'Secret'),
        h('input', {
          type: 'text',
          size: 32,
          placeholder: 'Enter a secret',
          value: this.state.secret,
          onChange: this.updateSecret,
        }),
      ]),
      h('div', [
        h('label', 'Lookup key'),
        h('input', {
          type: 'text',
          size: 32,
          placeholder: 'Identifier for the secret',
          value: this.state.lookupKey,
          onChange: this.updateLookupKey,
        }),
      ]),
      h(
        'button',
        { onClick: this.createBackup, disabled: !this.canBackup() },
        'Create backup'
      ),
      h('p', this.state.shareLink),
    ])
  }

  updateSecret(event) {
    this.setState({ secret: event.target.value })
  }

  updateLookupKey(event) {
    this.setState({ lookupKey: event.target.value })
  }

  // Are we able to backup yet?
  canBackup() {
    return (
      this.state.secret.length &&
      this.state.lookupKey.length &&
      this.props.selectedPublicKeys.length > 1
    )
  }

  createBackup() {
    // TODO add threshold (as last argument)
    const shareLink = dc.share(
      this.props.selectedPublicKeys,
      this.state.secret,
      this.state.lookupKey
    )
    this.setState({ shareLink, secret: '', lookupKey: '' })
  }
}
