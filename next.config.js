/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  cleanDistDir: false,
  assetPrefix: process.env.NODE_ENV === 'production' ? '/dc-web3-transport-react' : '',
  webpack (config) {
    config.experiments = { asyncWebAssembly: true }
    return config
  }
}

module.exports = nextConfig
