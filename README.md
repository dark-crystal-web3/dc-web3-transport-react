# dark crystal web3 transport react

An example nextjs / react project demonstrating [dark-crystal-web3-transport](https://gitlab.com/dark-crystal-web3/dark-crystal-web3-transport).

This shows sending and receiving example dark-crystal-web3 protocol messages over Waku-v2.

- Serve locally: `yarn dev`
- Build `yarn build`
- [Deployed to gitlab pages](https://dark-crystal-web3.gitlab.io/dc-web3-transport-react/?user=1)

When opening in the browser, we need to pass a 'user' as a querystring, eg: 'localhost:3000/?user=foo'

This allows us to 'be' multiple users in different tabs when testing, as it save credentials in localstorage with this user identifier string (can be anything).
